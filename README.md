Dripdexon
==============

A memory card game written as an experiment with
 * clutter
 * clutter-gtk
 * gtk3
 * rsvg
to create a rescalable full vector graphic game.

Theme images are from:
 * Monuments - http://www.flaticon.com/packs/monuments - CC BY 3.0
 * Humans - http://www.flaticon.com/packs/humans-2 - CC BY 3.0
 * Wildlife - http://www.flaticon.com/packs/wildlife - CC BY 3.0
 * Cartoonimals - http://openclipart.org/user-cliparts/lemmling - PD
                - http://openclipart.org/user-cliparts/StudioFibonacci - PD
 * Cards - http://nicubunu.ro/cards/?gallery=ornamental - PD
 * Events - http://clipart.nicubunu.ro/?gallery=events - PD
 * Map - http://clipart.nicubunu.ro/?gallery=rpg_map - PD
 * Pumpkins - http://clipart.nicubunu.ro/?gallery=carved-pumpkins - PD
 * Ghosts - http://clipart.nicubunu.ro/?gallery=pacman - PD
 * Flags - http://sourceforge.net/projects/sodipodi/files/sodipodi-clipart/flags-1.6/ - PD
                
Icon image based on the CC-BY 3.0 linked rings icon from Lorc - http://game-icons.net/lorc/originals/linked-rings.html
