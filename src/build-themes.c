/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * build-themes.c
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * build-themes.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * build-themes.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "build-themes.h"

static GSList* get_image_list(const gchar * directory) {
  GFile * folder = g_file_new_for_path (directory);
  GFileEnumerator * enumerator = g_file_enumerate_children (folder, G_FILE_ATTRIBUTE_STANDARD_NAME, G_FILE_QUERY_INFO_NONE, NULL, NULL);
  GSList * images = NULL;
  GFileInfo *file = NULL;
  while (( file = g_file_enumerator_next_file (enumerator, NULL, NULL)) != NULL) {
    gchar * filename = g_file_info_get_name (file);
    if (g_str_has_suffix(filename, ".svg")) {
      images = g_slist_append (images, filename);
    }
  }
  return images;
}

DripdexonTheme* build_default_theme () {
  GSList *image_list = get_image_list (DRIPDEXON_DATA_DIR"/themes/Monuments");
  DripdexonTheme *theme = dripdexon_theme_new_with_settings ("Monuments", DRIPDEXON_DATA_DIR"/themes/Monuments", image_list, PRIMARY_COLOR, SECONDARY_COLOR, BACKGROUND_COLOR);
  return theme;
}

DripdexonTheme* build_animals_theme () {
  GSList *image_list = get_image_list (DRIPDEXON_DATA_DIR"/themes/Wildlife");
  DripdexonTheme *theme = dripdexon_theme_new_with_settings ("Wildlife", DRIPDEXON_DATA_DIR"/themes/Wildlife", image_list, CLUTTER_COLOR_LightPlum, CLUTTER_COLOR_Aluminium2, BACKGROUND_COLOR);
  return theme;
}

DripdexonTheme* build_cartoonimals_theme () {
  GSList *image_list = get_image_list (DRIPDEXON_DATA_DIR"/themes/Cartoonimals");
  DripdexonTheme *theme = dripdexon_theme_new_with_settings ("Cartoonimals", DRIPDEXON_DATA_DIR"/themes/Cartoonimals", image_list, CLUTTER_COLOR_Gray,  CLUTTER_COLOR_LightOrange, CLUTTER_COLOR_Transparent);
  return theme;
}


DripdexonTheme* build_humans_theme () {
  GSList *image_list = get_image_list (DRIPDEXON_DATA_DIR"/themes/Humans");
  DripdexonTheme *theme = dripdexon_theme_new_with_settings ("Humans", DRIPDEXON_DATA_DIR"/themes/Humans", image_list, CLUTTER_COLOR_LightSkyBlue, CLUTTER_COLOR_LightOrange, BACKGROUND_COLOR);
  return theme;
}

DripdexonTheme* build_events_theme () {
  GSList *image_list = get_image_list (DRIPDEXON_DATA_DIR"/themes/Events");
  DripdexonTheme *theme = dripdexon_theme_new_with_settings ("Events", DRIPDEXON_DATA_DIR"/themes/Events", image_list, CLUTTER_COLOR_Aluminium5, CLUTTER_COLOR_Aluminium3, BACKGROUND_COLOR);
  return theme;
}

DripdexonTheme* build_cards_theme () {
  GSList *image_list = get_image_list (DRIPDEXON_DATA_DIR"/themes/Cards");
  DripdexonTheme *theme = dripdexon_theme_new_with_settings ("Cards", DRIPDEXON_DATA_DIR"/themes/Cards", image_list, CLUTTER_COLOR_LightGray, CLUTTER_COLOR_LightGray, BACKGROUND_COLOR);
  return theme;
}

DripdexonTheme* build_map_theme () {
  GSList *image_list = get_image_list (DRIPDEXON_DATA_DIR"/themes/Map");
  DripdexonTheme *theme = dripdexon_theme_new_with_settings ("Map", DRIPDEXON_DATA_DIR"/themes/Map", image_list, CLUTTER_COLOR_Aluminium3, CLUTTER_COLOR_LightChocolate, BACKGROUND_COLOR);
  return theme;
}

DripdexonTheme* build_pumpkins_theme () {
  GSList *image_list = get_image_list (DRIPDEXON_DATA_DIR"/themes/Pumpkins");
  DripdexonTheme *theme = dripdexon_theme_new_with_settings ("Pumpkins", DRIPDEXON_DATA_DIR"/themes/Pumpkins", image_list, CLUTTER_COLOR_DarkPlum, CLUTTER_COLOR_DarkPlum, BACKGROUND_COLOR);
  return theme;
}

DripdexonTheme* build_ghosts_theme () {
  GSList *image_list = get_image_list (DRIPDEXON_DATA_DIR"/themes/Ghosts");
  DripdexonTheme *theme = dripdexon_theme_new_with_settings ("Ghosts", DRIPDEXON_DATA_DIR"/themes/Ghosts", image_list, CLUTTER_COLOR_DarkSkyBlue, CLUTTER_COLOR_Orange, BACKGROUND_COLOR);
  return theme;
}

DripdexonTheme* build_flags_theme () {
  GSList *image_list = get_image_list (DRIPDEXON_DATA_DIR"/themes/Flags");
  DripdexonTheme *theme = dripdexon_theme_new_with_settings ("Flags", DRIPDEXON_DATA_DIR"/themes/Flags", image_list, CLUTTER_COLOR_DarkGreen, CLUTTER_COLOR_Transparent, BACKGROUND_COLOR);
  return theme;
}
