/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * build-themes.h
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * build-themes.h is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * build-themes.h is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __BUILD_THEMES_H__
#define __BUILD_THEMES_H__

#include <config.h>
#include "dripdexon-theme.h"
#include "dripdexon-setup.h"

DripdexonTheme* build_animals_theme ();
DripdexonTheme* build_default_theme ();
DripdexonTheme* build_humans_theme ();
DripdexonTheme* build_cartoonimals_theme ();
DripdexonTheme* build_cards_theme ();
DripdexonTheme* build_events_theme ();
DripdexonTheme* build_map_theme ();
DripdexonTheme* build_pumpkins_theme ();
DripdexonTheme* build_ghosts_theme ();
DripdexonTheme* build_flags_theme ();

#endif /*  __BUILD_THEMES_H__ */
