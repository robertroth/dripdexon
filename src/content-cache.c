/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * content-cache.c
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * content-cache.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * content-cache.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "content-cache.h"
#include "dripdexon-setup.h"

gboolean card_svg_draw(ClutterCanvas *canvas, cairo_t *cr, gint width, gint height, RsvgHandle *svg)
{
  RsvgDimensionData dim;
  rsvg_handle_get_dimensions(svg, &dim);
  gdouble img_size = MAX(dim.width, dim.height);
  gdouble holder_size = MIN(width,height);
  guint padding = MAX(holder_size/60, 6);
  gdouble scale_factor = (holder_size-2*padding)/(img_size);
  guint centerX = 0, centerY = 0;
  cairo_save(cr);
  cairo_set_operator(cr, CAIRO_OPERATOR_CLEAR);
  cairo_paint(cr);
  cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
  if (dim.width != dim.height) {
    centerX = holder_size - dim.width*scale_factor - 2*padding;
    centerY = holder_size - dim.height*scale_factor - 2*padding;
  }
  cairo_translate(cr, padding+(width-holder_size+centerX)/2, padding+(height-holder_size+centerY)/2);
  
  cairo_scale(cr, scale_factor, scale_factor);
  rsvg_handle_render_cairo(svg, cr);
  cairo_stroke(cr);
  cairo_restore(cr);
  return TRUE;
}

void cache_image(GHashTable* content_cache, gchar *image, gchar *theme, const gchar *path)
{
  if (!g_hash_table_contains(content_cache, image)) {
    GError *error = NULL;
    gchar* filename = g_build_filename(path, image, NULL);
    RsvgHandle* svg= rsvg_handle_new_from_file (filename, &error);
    if (error) {
      printf("Error loading %s : %s\n", image, error->message);
    }
    g_free (filename);
    
    ClutterContent* svg_content = clutter_canvas_new();
    g_signal_connect(svg_content, "draw", G_CALLBACK(card_svg_draw), svg);
    
    g_hash_table_insert(content_cache, image, svg_content);
    
  }
}

