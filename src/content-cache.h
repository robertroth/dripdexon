/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * content-cache.h
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * content-cache.h is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * content-cache.h is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __CONTENT_CACHE_H__
#define __CONTENT_CACHE_H__

#include <config.h>
#include <librsvg/rsvg.h>
#include <cairo/cairo.h>
#include <clutter/clutter.h>

void cache_image(GHashTable* content_cache, gchar *image, gchar *theme, const gchar *path);

gboolean card_svg_draw (ClutterCanvas *canvas, cairo_t *cr, gint width, gint height, RsvgHandle *svg);

#endif /*  __CONTENT_CACHE_H__ */
