/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * dripdexon-card.c
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * dripdexon-card.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * dripdexon-card.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "dripdexon-card.h"

struct _DripdexonCardPrivate
{
  gboolean flipped;
  GHashTable* content_cache;
  gboolean active;
  DripdexonTheme *theme;
};

enum
{
  PROP_0,
  PROP_FLIPPED, 
  PROP_CONTENT_CACHE,
  PROP_THEME,
  PROP_LAST
};

enum
{
  FLIPPED,
  LAST_SIGNAL
};

static GParamSpec *obj_props [PROP_LAST] = { NULL, };
static guint       obj_signals [LAST_SIGNAL] = { 0, };

G_DEFINE_TYPE (DripdexonCard, dripdexon_card, CLUTTER_TYPE_ACTOR)

static void
dripdexon_card_set_property (GObject *object, 
                             guint prop_id, 
                             const GValue *value, 
                             GParamSpec *pspec)
{
  DripdexonCard *card = DRIPDEXON_CARD (object);
  
  switch (prop_id) 
  {
    case PROP_FLIPPED:
      dripdexon_card_set_flipped (card, g_value_get_boolean(value));
      break;
    case PROP_CONTENT_CACHE:
      dripdexon_card_set_content_cache (card, g_value_get_pointer (value));
      break;
    case PROP_THEME:
      dripdexon_card_set_theme (card, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  } 
}

static void
dripdexon_card_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  DripdexonCard *card = DRIPDEXON_CARD (object);

  switch (prop_id)
    {
    case PROP_FLIPPED:
      g_value_set_boolean (value, card->priv->flipped);
      break;
    case PROP_CONTENT_CACHE:
      g_value_set_pointer (value, card->priv->content_cache);
      break;
    case PROP_THEME:
      g_value_set_object (value, dripdexon_card_get_theme (card));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void dripdexon_card_class_init (DripdexonCardClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  gobject_class->set_property = dripdexon_card_set_property;
  gobject_class->get_property = dripdexon_card_get_property;
  g_type_class_add_private (klass, sizeof (DripdexonCardPrivate));
  obj_props[PROP_FLIPPED] =
    g_param_spec_boolean ("flipped",
                        "flipped",
                        "Is the card showing the image or not",
                        FALSE,
                        G_PARAM_READABLE | G_PARAM_WRITABLE);
  obj_props[PROP_CONTENT_CACHE] = 
    g_param_spec_pointer ("content-cache",
                         "content-cache",
                         "Hashtable with the image cache to use",
                         G_PARAM_READABLE | G_PARAM_WRITABLE);
  obj_props[PROP_THEME] = 
    g_param_spec_object ("theme",
                         "theme",
                         "Theme used",
                         DRIPDEXON_TYPE_THEME, 
                         G_PARAM_READABLE | G_PARAM_WRITABLE);
  g_object_class_install_properties (gobject_class, PROP_LAST, obj_props);
  
  obj_signals [FLIPPED] =
    g_signal_newv ("flipped",
      G_TYPE_FROM_CLASS (gobject_class),
      G_SIGNAL_RUN_LAST,
      NULL, NULL, NULL,
      g_cclosure_marshal_VOID__VOID,
      G_TYPE_NONE, 0, NULL);
}


// show/hide the image content of the card
static void on_dripdexon_card_image_toggled (ClutterTimeline *timeline, gchar* marker_name,
                                             guint msecs, DripdexonCard* card)
{
  if (card->priv->flipped || !card->priv->active) {
    gfloat width, height;
  //printf("Show %s\n", clutter_actor_get_name(actor));
    clutter_actor_get_size (CLUTTER_ACTOR (card), &width, &height);
  
    ClutterContent *svg_content = g_hash_table_lookup(card->priv->content_cache, clutter_actor_get_name (CLUTTER_ACTOR (card)));
    clutter_canvas_set_size(CLUTTER_CANVAS(svg_content), width, height);
    clutter_actor_set_content(CLUTTER_ACTOR (card), svg_content);
    clutter_actor_set_content_gravity (CLUTTER_ACTOR (card), CLUTTER_CONTENT_GRAVITY_RESIZE_ASPECT);
  } else {
    clutter_actor_set_content (CLUTTER_ACTOR (card), NULL);
  }
  clutter_actor_set_reactive (CLUTTER_ACTOR (card), card->priv->active);
}

static void animate_flip (DripdexonCard *actor)
{
  ClutterTransition *transition_group = clutter_transition_group_new ();
  clutter_timeline_set_duration (CLUTTER_TIMELINE (transition_group), FLIP_DURATION);
  
  ClutterTransition *flip_transition = clutter_property_transition_new ("rotation-angle-y");
  clutter_actor_set_reactive (CLUTTER_ACTOR (actor), FALSE);
  gdouble original = clutter_actor_get_content (CLUTTER_ACTOR (actor)) ? 0.0 : 180.0;
  clutter_transition_set_from (CLUTTER_TRANSITION (flip_transition), G_TYPE_DOUBLE, original);
  clutter_transition_set_to (CLUTTER_TRANSITION (flip_transition), G_TYPE_DOUBLE, original + 180.0);
  clutter_timeline_set_duration (CLUTTER_TIMELINE (flip_transition), FLIP_DURATION);
  clutter_timeline_add_marker_at_time (CLUTTER_TIMELINE (flip_transition), "imagetoggle", FLIP_DURATION/2);
  g_signal_connect (flip_transition, "marker-reached::imagetoggle", G_CALLBACK (on_dripdexon_card_image_toggled), actor);
  
  clutter_transition_group_add_transition (CLUTTER_TRANSITION_GROUP (transition_group), flip_transition);
  
  clutter_actor_remove_transition (CLUTTER_ACTOR (actor), "flip");
  clutter_actor_add_transition (CLUTTER_ACTOR (actor), "flip", CLUTTER_TRANSITION (transition_group));
  clutter_timeline_start (CLUTTER_TIMELINE (transition_group));
}

static void animate_to_color (DripdexonCard *actor, const ClutterColor *target) 
{
  ClutterColor original;
  if ( CLUTTER_IS_ACTOR(actor)) {
    clutter_actor_get_background_color(CLUTTER_ACTOR(actor), &original);
    ClutterTransition *background_transition = clutter_property_transition_new("background-color");
    clutter_transition_set_from(CLUTTER_TRANSITION(background_transition), CLUTTER_TYPE_COLOR, &original);
    clutter_transition_set_to(CLUTTER_TRANSITION(background_transition), CLUTTER_TYPE_COLOR, target);
    clutter_transition_set_remove_on_complete(CLUTTER_TRANSITION(background_transition), TRUE);
    clutter_timeline_set_duration(CLUTTER_TIMELINE(background_transition), COLOR_FADE_DURATION);
    clutter_actor_remove_transition(CLUTTER_ACTOR(actor), "bg-fade");
    clutter_actor_add_transition(CLUTTER_ACTOR(actor), "bg-fade", background_transition);
    clutter_timeline_start(CLUTTER_TIMELINE(background_transition));
  }
}

// called on matched cards
void dripdexon_card_deactivate (DripdexonCard *actor) 
{
  //dripdexon_card_set_flipped (DRIPDEXON_CARD (actor), TRUE);
  //clutter_actor_set_rotation_angle (CLUTTER_ACTOR (actor), CLUTTER_Y_AXIS, actor->priv->flipped?180.0:0.0);
  actor->priv->active = FALSE;
  animate_to_color ( actor, dripdexon_theme_get_background_color (actor->priv->theme));
  clutter_actor_set_reactive( CLUTTER_ACTOR (actor), FALSE);
  on_dripdexon_card_image_toggled (NULL, NULL, 0, actor);
}

// required to do proper svg rescaling
static void card_resized (ClutterActor *actor, ClutterActorBox *box, ClutterAllocationFlags flags, gpointer user_data)
{
  ClutterCanvas* content = NULL;
  if ((content = CLUTTER_CANVAS(clutter_actor_get_content(actor))) != NULL) {
    clutter_canvas_set_size(content, box->x2 - box->x1, box->y2 - box->y1);
  }
}

static void on_card_clicked (ClutterClickAction *action, ClutterActor *actor, gpointer user_data)
{
  dripdexon_card_flip (DRIPDEXON_CARD(actor));
}

static void dripdexon_card_init (DripdexonCard *card)
{
  card->priv = DRIPDEXON_CARD_GET_PRIVATE (card);
  card->priv->flipped = FALSE;
  card->priv->content_cache = NULL;
  card->priv->active = TRUE;
  clutter_actor_set_reactive (CLUTTER_ACTOR (card), TRUE);
  
  clutter_actor_set_rotation_angle(CLUTTER_ACTOR (card), CLUTTER_Y_AXIS, 180.0);
  clutter_actor_set_pivot_point(CLUTTER_ACTOR (card), 0.5, 0.5);
  g_signal_connect(card, "allocation-changed", G_CALLBACK(card_resized), NULL);
  //g_signal_connect(card, "button-press-event", G_CALLBACK(card_button_event), NULL);
  //g_signal_connect(card, "button-release-event", G_CALLBACK(card_button_event), NULL);
  
  ClutterAction *action = clutter_click_action_new ();
  clutter_actor_add_action (CLUTTER_ACTOR (card), action);
  g_signal_connect (action, "clicked", G_CALLBACK (on_card_clicked), NULL);

}

// flip the card (flip animation, color change, property update, signal emit)
void
dripdexon_card_set_flipped (DripdexonCard *card, 
                            gboolean flipped)
{
  g_return_if_fail (DRIPDEXON_IS_CARD (card));
  if (flipped == card->priv->flipped) 
    return;
    
  animate_flip(card);
  ClutterColor *result;
  if (flipped) {
    result = dripdexon_theme_get_secondary_color (card->priv->theme);
  } else {
    result = dripdexon_theme_get_primary_color (card->priv->theme);
  }
  animate_to_color(card, result);
  clutter_color_free (result);
  card->priv->flipped = flipped;
  g_signal_emit ( card, obj_signals[FLIPPED], 0);
}

void dripdexon_card_flip (DripdexonCard *card)
{
  g_return_if_fail (DRIPDEXON_IS_CARD (card));
  if (!card->priv->flipped) {
    dripdexon_card_set_flipped (card, !card->priv->flipped);
  }
}

gboolean 
dripdexon_card_is_flipped (DripdexonCard *card)
{
  return card->priv->flipped;
}

void
dripdexon_card_set_content_cache (DripdexonCard *card, 
                                  GHashTable *content_cache)
{
  g_return_if_fail (DRIPDEXON_IS_CARD (card));
  card->priv->content_cache = content_cache;
}

GHashTable* 
dripdexon_card_get_content_cache (DripdexonCard *card)
{
  return card->priv->content_cache;
}

void
dripdexon_card_set_theme (DripdexonCard* card, DripdexonTheme* theme) {
  card->priv->theme = theme;
  clutter_actor_set_background_color (CLUTTER_ACTOR (card), dripdexon_theme_get_primary_color (card->priv->theme));
}

DripdexonTheme *dripdexon_card_get_theme (DripdexonCard *card) {
  return card->priv->theme;
}
