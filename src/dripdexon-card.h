/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * dripdexon-card.h
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * dripdexon-card.h is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * dripdexon-card.h is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DRIPDEXON_CARD_H__
#define __DRIPDEXON_CARD_H__

#include <config.h>
#include <math.h>
#include <stdlib.h> 
#include <glib-object.h>
#include <clutter/clutter.h>
#include <librsvg/rsvg.h>
#include <cairo/cairo.h>
#include "dripdexon-setup.h"
#include "dripdexon-theme.h"

#define LIGHTEN_FACTOR      1.5
#define COLOR_FADE_DURATION 200
#define FLIP_DURATION       400

G_BEGIN_DECLS

#define DRIPDEXON_TYPE_CARD                  (dripdexon_card_get_type ())
#define DRIPDEXON_CARD(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRIPDEXON_TYPE_CARD, DripdexonCard))
#define DRIPDEXON_IS_CARD(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRIPDEXON_TYPE_CARD))
#define DRIPDEXON_CARD_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), DRIPDEXON_TYPE_CARD, DripdexonCardClass))
#define DRIPDEXON_IS_CARD_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), DRIPDEXON_TYPE_CARD))
#define DRIPDEXON_CARD_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), DRIPDEXON_TYPE_CARD, DripdexonCardClass))
#define DRIPDEXON_CARD_GET_PRIVATE(obj)      (G_TYPE_INSTANCE_GET_PRIVATE ((obj), DRIPDEXON_TYPE_CARD, DripdexonCardPrivate))

typedef struct _DripdexonCard   DripdexonCard;
typedef struct _DripdexonCardClass   DripdexonCardClass;
typedef struct _DripdexonCardPrivate   DripdexonCardPrivate;

struct _DripdexonCard
{
  ClutterActor parent_instance;
  DripdexonCardPrivate *priv;
};

struct _DripdexonCardClass
{
  ClutterActorClass parent_class;
};

GType dripdexon_card_get_type(void);

void dripdexon_card_set_flipped (DripdexonCard *card, 
                                 gboolean flipped);
 
                          
void dripdexon_card_flip (DripdexonCard *card);

gboolean dripdexon_card_is_flipped (DripdexonCard *card);

void dripdexon_card_deactivate (DripdexonCard *card);

void dripdexon_card_set_content_cache (DripdexonCard *card, 
                                       GHashTable* content_cache);
                           
GHashTable* dripdexon_card_get_content_cache (DripdexonCard *card);

void dripdexon_card_set_theme (DripdexonCard *card, DripdexonTheme *theme);

DripdexonTheme* dripdexon_card_get_theme (DripdexonCard * card);

G_END_DECLS

#endif /*  __DRIPDEXON_CARD_H__ */
