/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * dripdexon-main.c
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * dripdexon-main.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * dripdexon-main.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <stdlib.h>
#include <math.h>

#include <gtk/gtk.h>

#include "dripdexon-setup.h"
#include "dripdexon-size-selector.h"
#include <clutter-gtk/clutter-gtk.h>
#include <clutter/clutter.h>
#include <glib/gi18n.h>

#include "dripdexon-card.h"
#include "dripdexon-stage.h"
#include "build-themes.h"

static gboolean fullscreen = FALSE;
static GtkWidget *clock_label = NULL;
static GtkWidget *pause_button = NULL;
static GtkWidget *restart_button = NULL;
static GtkWidget *flips_label = NULL;
static GtkWidget *pairs_label = NULL;
static DripdexonStage *stage = NULL;
static ClutterActor *clutter_stage = NULL;
static guint tick = 0;
static DripdexonTheme *THEMES[10];
static DripdexonTheme *selected;

static guint NEWGAME_MODE = NEWGAME_MODE_RANDOM_THEME;

static void
on_fullscreen (GtkButton *button,
               GtkWindow *window)
{
  if (!fullscreen)
    {
      gtk_window_fullscreen (window);
      fullscreen = TRUE;
    }
  else
    {
      gtk_window_unfullscreen (window);
      fullscreen = FALSE;
      //clutter_actor_queue_relayout (clutter_stage);
    }
}

static void update_status (DripdexonStage *stage)
{
  gchar * markup;
  gboolean started = dripdexon_stage_is_started(stage);
  if (started) {
    markup = g_strdup_printf (_("%d"), dripdexon_stage_get_flips (stage));
  } else {
    markup = g_strdup_printf (_("-"));
  }
  gtk_label_set_markup (GTK_LABEL (flips_label), markup);
  g_free (markup);
  
  if (started) {
    markup = g_strdup_printf (_("%d"), dripdexon_stage_get_pairs (stage));
  } else {
    markup = g_strdup_printf (_("-"));
  }
  gtk_label_set_markup (GTK_LABEL (pairs_label), markup);
  g_free (markup);
  
}

static gboolean timer_tick (gpointer user_data)
{
  DripdexonStage *stage = DRIPDEXON_STAGE (user_data);
  if (dripdexon_stage_is_paused (stage)) {
    return FALSE;
  }
  guint elapsed = dripdexon_stage_get_elapsed (stage);
  guint hours = elapsed / 3600;
  guint minutes = (elapsed - hours * 3600 ) / 60;
  guint seconds = elapsed - hours * 3600  - minutes * 60;
  gchar * time = NULL;
  if (hours > 0) {
    time = g_strdup_printf (_("%02d:%02d:%02d"), hours, minutes, seconds);
  } else {
    time = g_strdup_printf (_("%02d:%02d"), minutes, seconds);
  }
  
  gtk_label_set_markup (GTK_LABEL (clock_label), time);
  g_free (time);
  return dripdexon_stage_is_started (stage) && !dripdexon_stage_is_paused (stage);
}

static void on_started (DripdexonStage *stage, gpointer user_data) {
  timer_tick (stage);
  if (tick == 0) {
    tick = g_timeout_add_seconds (1, (GSourceFunc)timer_tick, stage);
  }
  gtk_widget_set_sensitive (pause_button, TRUE);
  gtk_image_set_from_icon_name (GTK_IMAGE (gtk_button_get_image (GTK_BUTTON (pause_button))), "media-playback-pause", GTK_ICON_SIZE_LARGE_TOOLBAR);
}

static void final_animate (ClutterActor *actor, gpointer user_data) {
  
  ClutterTransition *group = clutter_transition_group_new ();
  
  clutter_actor_set_pivot_point (actor, 0.5, 0.5);
  
  ClutterTransition *rotation = clutter_property_transition_new ("rotation-angle-z");
  ClutterInterval *rotation_interval = clutter_interval_new (G_TYPE_DOUBLE, 0.0, 720.0);
  clutter_transition_set_interval (rotation, rotation_interval);
  clutter_timeline_set_duration (CLUTTER_TIMELINE (rotation), FLIP_DURATION * 5);
  clutter_transition_group_add_transition ( CLUTTER_TRANSITION_GROUP (group), rotation);
  
  ClutterTransition *resizex = clutter_property_transition_new ("scale-x");
  ClutterInterval *resizex_interval = clutter_interval_new (G_TYPE_DOUBLE, 1.0, 0.00);
  clutter_transition_set_interval (resizex, resizex_interval);
  clutter_timeline_set_duration (CLUTTER_TIMELINE (resizex), FLIP_DURATION * 5);
  clutter_transition_group_add_transition ( CLUTTER_TRANSITION_GROUP (group), resizex);
  
  ClutterTransition *resizey = clutter_property_transition_new ("scale-y");
  ClutterInterval *resizey_interval = clutter_interval_new (G_TYPE_DOUBLE, 1.0, 0.00);
  clutter_transition_set_interval (resizey, resizey_interval);
  clutter_timeline_set_duration (CLUTTER_TIMELINE (resizey), FLIP_DURATION * 5);
  clutter_transition_group_add_transition ( CLUTTER_TRANSITION_GROUP (group), resizey);
  
  clutter_timeline_set_duration (CLUTTER_TIMELINE (group), FLIP_DURATION * 5);
  clutter_actor_remove_transition (actor, "fade");
  clutter_actor_add_transition (actor, "fade", group);
  clutter_timeline_start (CLUTTER_TIMELINE (group));
  
}

static ClutterActor* create_text (const gchar *string, const ClutterColor* color, guint size) {
  ClutterActor* text  = clutter_text_new ();
  gchar * markup = g_strdup_printf ("<span font-weight='bold' font='%d'>%s</span>", size, string);
  clutter_text_set_markup (CLUTTER_TEXT (text), markup);
  g_free (markup);
  clutter_text_set_line_alignment (CLUTTER_TEXT (text), PANGO_ALIGN_CENTER);
  if (color)
    clutter_text_set_color (CLUTTER_TEXT (text), color);
  clutter_text_set_line_wrap (CLUTTER_TEXT (text), TRUE);
  return text;
}

static void on_pause (DripdexonStage *stage, gpointer user_data) {
  gtk_image_set_from_icon_name (GTK_IMAGE (gtk_button_get_image (GTK_BUTTON (pause_button))), "media-playback-start", GTK_ICON_SIZE_LARGE_TOOLBAR);
  tick = 0;
  if (user_data) {
    // game is finished
    ClutterActor* clutter_stage = CLUTTER_ACTOR (user_data);
    ClutterActor *table = clutter_actor_get_child_at_index(clutter_stage, 0);
    g_list_foreach (clutter_actor_get_children (table), (GFunc)final_animate, GINT_TO_POINTER (TRUE));
    gchar * text = g_strdup_printf (_("Congratulations!\n You have completed the game in %d flips!"), dripdexon_stage_get_flips (stage));
    ClutterActor * text_actor = create_text (text, NULL, 24);
    g_free (text);
    //ClutterActor* fade = clutter_actor_new ();
    //ClutterColor *fade_color = clutter_color_new (50, 50, 50, 180);
   // clutter_actor_set_background_color (fade, fade_color);
    //clutter_color_free (fade_color);
    //clutter_actor_add_child (clutter_stage, fade);
    clutter_actor_add_child (clutter_stage, CLUTTER_ACTOR (text_actor));
    
    //clutter_actor_add_constraint (fade, clutter_bind_constraint_new (clutter_stage, CLUTTER_BIND_ALL, 0));
    clutter_actor_add_constraint (CLUTTER_ACTOR (text_actor), clutter_align_constraint_new (clutter_stage, CLUTTER_ALIGN_BOTH, 0.5));
    gtk_widget_set_sensitive (pause_button, FALSE);
  }
}

static void on_match (DripdexonStage* stage, gpointer user_data) {
  update_status (stage);
}

static void on_reveal (DripdexonStage *stage, DripdexonCard *card, gpointer user_data) {
  update_status (stage);
}

static void pause_clicked (GtkButton *button) {
  if (dripdexon_stage_is_paused (stage)) {
    dripdexon_stage_resume (DRIPDEXON_STAGE (stage));
  } else {
    dripdexon_stage_pause (DRIPDEXON_STAGE (stage));
  }
}

static void setup_stage (ClutterActor *clutter_stage, guint sizeX, guint sizeY) {
  if (!stage) {
    THEMES[0] = build_default_theme ();
    THEMES[1] = build_animals_theme ();
    THEMES[2] = build_humans_theme ();
    THEMES[3] = build_cartoonimals_theme ();
    THEMES[4] = build_cards_theme ();
    THEMES[5] = build_events_theme ();
    THEMES[6] = build_map_theme ();
    THEMES[7] = build_pumpkins_theme ();
    THEMES[8] = build_ghosts_theme ();
    THEMES[9] = build_flags_theme ();
    stage = g_object_new(DRIPDEXON_TYPE_STAGE, "width", sizeX, "height", sizeY, "theme", THEMES[0], NULL);
    g_signal_connect (pause_button, "clicked", G_CALLBACK (pause_clicked), stage);
    g_signal_connect (stage, "started", G_CALLBACK(on_started), GINT_TO_POINTER(FALSE));
    g_signal_connect (stage, "resumed", G_CALLBACK(on_started), GINT_TO_POINTER(TRUE));
    g_signal_connect (stage, "paused", G_CALLBACK(on_pause), NULL);
    g_signal_connect (stage, "done", G_CALLBACK(on_pause), clutter_stage);
    g_signal_connect (stage, "matched", G_CALLBACK(on_match), NULL);
    g_signal_connect (stage, "revealed", G_CALLBACK (on_reveal), NULL);
  } else {
    dripdexon_stage_set_width (stage, sizeX);
    dripdexon_stage_set_height (stage, sizeY);
    dripdexon_stage_set_theme (stage, selected);
  }
  dripdexon_stage_initialize (stage, clutter_stage);
  
}

static ClutterActor* stage_prepare (GtkWidget* clutter, guint sizeX, guint sizeY) {
  
  ClutterActor *clutter_stage = gtk_clutter_embed_get_stage (GTK_CLUTTER_EMBED (clutter));
  clutter_actor_set_background_color (clutter_stage, CLUTTER_COLOR_Aluminium1);
  clutter_actor_set_layout_manager (clutter_stage, clutter_bin_layout_new (0.5, 0.5));
  setup_stage (clutter_stage, sizeX, sizeY);
  return clutter_stage;
}

static void on_theme_selected (GtkIconView *icon_view, GtkTreeModel *model) {
  GList * selected_list = gtk_icon_view_get_selected_items (icon_view);
  GtkTreePath *path = g_list_nth_data (selected_list, 0);
  if (path) {
    GtkTreeIter iter;
    gtk_tree_model_get_iter (model, &iter, path);
    gtk_tree_model_get (model, &iter, 2, &selected, -1);
    //printf ("Selected %s\n", dripdexon_theme_get_name (&selected));
  }
  g_list_free_full (selected_list, (GDestroyNotify) gtk_tree_path_free);
}

static GtkWidget* build_newgame_dialog (GtkWidget *window) {
  GtkBuilder *builder = gtk_builder_new();
  gtk_builder_add_from_resource (builder, "/org/gogames/dripdexon/data/ui/newgame.ui", NULL);
  GtkWidget* newgame_dialog = GTK_WIDGET (gtk_builder_get_object (builder, "newgame_dialog"));
  gtk_window_set_transient_for (GTK_WINDOW (newgame_dialog), GTK_WINDOW (window));
  //gtk_widget_set_parent_window (newgame_dialog, gtk_widget_get_window(window));
  GtkWidget *theme_selector, *size_selector, *size_frame;
  GtkTreeModel *model;
  GtkTreeIter iter;
  
  gtk_dialog_set_default_response (GTK_DIALOG (newgame_dialog), GTK_RESPONSE_ACCEPT);
  theme_selector = GTK_WIDGET (gtk_builder_get_object (builder, "themes_view"));
  model = gtk_icon_view_get_model (GTK_ICON_VIEW (theme_selector));
  guint i;
  guint selected_index = 0;
  for (i = 0; i< sizeof (THEMES)/sizeof(DripdexonTheme*);i++) {
    gtk_list_store_append (GTK_LIST_STORE (model), &iter );
    if (selected == THEMES[i]) {
      selected_index = i;
    }
    gtk_list_store_set (GTK_LIST_STORE (model), &iter, 0, dripdexon_theme_get_name (THEMES[i]), 1, dripdexon_theme_get_preview_image (THEMES[i]), 2, THEMES[i], -1);
  }
  g_signal_connect (theme_selector, "selection-changed", G_CALLBACK (on_theme_selected), model);
  gtk_icon_view_set_markup_column (GTK_ICON_VIEW (theme_selector), 0);
  gtk_icon_view_set_pixbuf_column (GTK_ICON_VIEW (theme_selector), 1);
  gtk_icon_view_set_item_width (GTK_ICON_VIEW (theme_selector), 96);
  GtkTreePath *first = gtk_tree_path_new_from_indices (selected_index, -1);
  gtk_icon_view_select_path (GTK_ICON_VIEW (theme_selector), first);
  gtk_tree_path_free (first);
  size_frame = GTK_WIDGET (gtk_builder_get_object (builder, "size_alignment"));
  size_selector = gtk_clutter_embed_new ();
  ClutterActor * selector_stage = gtk_clutter_embed_get_stage (GTK_CLUTTER_EMBED (size_selector));
  initialize_size_selector (selector_stage);
//  g_signal_connect (size_selector, "draw", G_CALLBACK (size_selector_draw), NULL);
  gtk_widget_set_size_request (size_selector, 20*MAXGRIDWIDTH, 20*MAXGRIDHEIGHT);
  gtk_container_add (GTK_CONTAINER (size_frame), size_selector);
  gtk_widget_show_all (newgame_dialog);
  g_object_unref (builder);
  return newgame_dialog;
}

static void restart_clicked (GtkButton *button, gpointer user_data) {
  guint sizeX, sizeY;
  gboolean newgame = TRUE;
  guint images = 0;
  switch (NEWGAME_MODE) {
    case NEWGAME_MODE_CUSTOMIZE:
    dripdexon_stage_pause(stage);
    GtkWidget *newgame_dialog = build_newgame_dialog (gtk_widget_get_toplevel (GTK_WIDGET (button)));
    guint response = gtk_dialog_run (GTK_DIALOG (newgame_dialog));
    switch (response) {
      case GTK_RESPONSE_ACCEPT:
      get_selected_size (&sizeX, &sizeY);
      break;
      default:
      newgame = FALSE;
      dripdexon_stage_resume(stage);
      break;
    }
    gtk_widget_destroy (newgame_dialog);
    break;
    case NEWGAME_MODE_LAST_SETTING:
    selected = dripdexon_stage_get_theme (stage);
    dripdexon_stage_get_size (stage, &sizeX, &sizeY);
    break;
    case NEWGAME_MODE_RANDOM_THEME:
    selected = THEMES[rand()%(sizeof(THEMES)/sizeof(DripdexonTheme*))];
    dripdexon_stage_get_size (stage, &sizeX, &sizeY);
    break;
    case NEWGAME_MODE_SIZE_FIT_THEME:
    selected = dripdexon_stage_get_theme (stage);
    images = dripdexon_theme_get_image_count(selected);
    sizeY = 2*images/5/2;
    sizeX = 3*images/5/2;
    break;
  }
  if (newgame) {
    setup_stage (clutter_stage, sizeX, sizeY);
    update_status (stage);
    if (tick>0) {
      g_source_remove (tick);
      tick = 0;
    }
    gtk_widget_set_sensitive (pause_button, FALSE);
    gtk_label_set_markup (GTK_LABEL (clock_label), _("00:00"));
  }
  
}

static gboolean restart_released (GtkButton *button, GdkEventButton *event, gpointer user_data) {
  if (event->button != 1) {
    NEWGAME_MODE = NEWGAME_MODE_CUSTOMIZE;
  }
  restart_clicked(restart_button, NULL);
  NEWGAME_MODE = NEWGAME_MODE_RANDOM_THEME;
  return TRUE;
}

static void setup_ui () {
  GtkWidget *window, *clutter;
  GtkWidget *vbox;
  GtkBuilder *builder = gtk_builder_new();
  gtk_builder_add_from_resource (builder, "/org/gogames/dripdexon/data/ui/dripdexon.ui", NULL);
  gtk_builder_connect_signals (builder, NULL);
  window = GTK_WIDGET (gtk_builder_get_object (builder, "main_window"));
  gtk_window_set_icon_name (GTK_WINDOW (window),  "dripdexon.svg");
  vbox = GTK_WIDGET (gtk_builder_get_object (builder, "vbox"));

  clutter = gtk_clutter_embed_new ();
  gtk_box_pack_start (GTK_BOX (vbox), clutter, TRUE, TRUE, 6);

  restart_button = GTK_WIDGET (gtk_builder_get_object (builder, "restart_button"));
  //g_signal_connect (restart_button, "clicked", G_CALLBACK (restart_clicked), NULL);
  g_signal_connect (restart_button, "button-release-event", G_CALLBACK (restart_released), NULL);
  
  pause_button = GTK_WIDGET (gtk_builder_get_object (builder, "pause_button"));
  clock_label = GTK_WIDGET (gtk_builder_get_object (builder, "clock_label"));

  gtk_widget_set_sensitive (pause_button, FALSE);
  
  flips_label = GTK_WIDGET (gtk_builder_get_object (builder, "flipped_count"));
  
  pairs_label = GTK_WIDGET (gtk_builder_get_object (builder, "matched_count"));
  
  gtk_widget_show_all (window);
  clutter_stage = stage_prepare(clutter, GRIDWIDTH, GRIDHEIGHT);
  update_status(stage);
  
  g_object_unref (G_OBJECT (builder));
}

int
main (int argc, char *argv[])
{
  GError *error = NULL;
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, DRIPDEXON_LOCALEDIR);
  textdomain (PACKAGE);
  if (gtk_clutter_init_with_args (&argc, &argv,
                                  NULL,
                                  NULL,
                                  NULL,
                                  &error) != CLUTTER_INIT_SUCCESS)
    {
      if (error)
        {
          g_critical ("Unable to initialize Clutter-GTK: %s", error->message);
          g_error_free (error);
          return EXIT_FAILURE;
        }
      else
        g_error ("Unable to initialize Clutter-GTK");
    }

  /* calling gtk_clutter_init* multiple times should be safe */
  g_assert (gtk_clutter_init (NULL, NULL) == CLUTTER_INIT_SUCCESS);
  
  setup_ui ();
  
  gtk_main ();

  return 0;
}
