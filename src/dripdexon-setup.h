/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * dripdexon-setup.h
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * dripdexon-setup.h is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * dripdexon-setup.h is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __DRIPDEXON_SETUP_H__
#define __DRIPDEXON_SETUP_H__

#include <config.h>
#include <clutter/clutter.h>

#define MAXGRIDWIDTH    21
#define MAXGRIDHEIGHT   14
#define GRIDWIDTH       10
#define GRIDHEIGHT      6

#define NEWGAME_MODE_LAST_SETTING 0
#define NEWGAME_MODE_RANDOM_THEME 1
#define NEWGAME_MODE_CUSTOMIZE 2
#define NEWGAME_MODE_SIZE_FIT_THEME 3
#define SPACING         6
#define WINWIDTH        600
#define WINHEIGHT       330
#define PRIMARY_COLOR   CLUTTER_COLOR_SkyBlue
#define SECONDARY_COLOR CLUTTER_COLOR_LightSkyBlue
#define BACKGROUND_COLOR CLUTTER_COLOR_Aluminium1

#endif /*  __DRIPDEXON_SETUP_H__ */
