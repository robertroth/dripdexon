/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * dripdexon-size-selector.c
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * dripdexon-size-selector.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * dripdexon-size-selector.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "dripdexon-size-selector.h"

static ClutterActor *selactors [MAXGRIDHEIGHT][MAXGRIDWIDTH];
static ClutterActor *selected;
static ClutterActor *sizetext;
static guint  aX, aY;
static guint sizeX = GRIDWIDTH -1, sizeY = GRIDHEIGHT -1;

static void get_child_indexes (ClutterActor* container, ClutterActor *child, guint *x, guint *y) {
  gfloat width, height, cx, cy;
  clutter_actor_get_size (container, &width, &height);
  clutter_actor_get_position (child, &cx, &cy);
  *x = (guint)floor(cx / (width / MAXGRIDWIDTH ));
  *y = (guint)floor(cy / (height/ MAXGRIDHEIGHT));
}

static void update_rect (ClutterActor* actor, ClutterActor* reference) {
  guint refX, refY, sX, sY;
  gboolean top, bottom, left, right, remainX, remainY, isValid, isSelected, isPossible;
  ClutterActor *container = clutter_actor_get_parent (actor);
  
  get_child_indexes (container, actor, &aX, &aY);
  get_child_indexes (container, selected, &sX, &sY);
  if (reference) {
    get_child_indexes (container, reference, &refX, &refY);
  } else {
    refX = sX;
    refY = sY;
  }
  left = (aX == 0 && aY <= sY);
  right = (aX == sX && aY <= sY);
  top = (aY == 0 && aX <= sX);
  bottom = (aY == sY && aX <= sX);
  remainX = (aX >= sX && aX <= refX && aY == 0);
  remainY = (aY >= sY && aY <= refY && aX == 0);
  isValid = (refX + 1) * (refY + 1)%2 == 0;
  isSelected = right || bottom || left || top;
  isPossible = (aX ==refX && aY <= refY) || (aY == refY && aX <= refX) || remainX || remainY;
  clutter_actor_set_background_color (actor, isSelected?PRIMARY_COLOR:isPossible?(isValid?CLUTTER_COLOR_LightGray:CLUTTER_COLOR_LightScarletRed):BACKGROUND_COLOR);
  if (refX == 0 && refY == 0 && aX == 0 && aY == 0) {
    clutter_actor_set_background_color (actor, CLUTTER_COLOR_LightScarletRed);
  }
}

static void update_size_text (guint sX, guint sY) {
  if (sX >= MAXGRIDWIDTH || sY >= MAXGRIDHEIGHT) 
    return;
  gchar *size_string = g_strdup_printf("<tt><b><span font='%d'>%2dx%2d</span></b></tt>", 16, sX+1, sY+1);
  clutter_text_set_markup (CLUTTER_TEXT (sizetext), size_string);
  g_free (size_string);
}

static void on_rect_entered (ClutterActor *actor, ClutterCrossingEvent *event, ClutterActor *container) {
  g_list_foreach (clutter_actor_get_children (container), (GFunc)update_rect, actor);
  //clutter_actor_queue_redraw (sizetext);
  guint sX, sY;
  get_child_indexes (container, actor, &sX, &sY);
  update_size_text (sX, sY);
  
}

static void on_rect_clicked (ClutterClickAction *click_action, ClutterActor* actor, ClutterActor* container) {
  get_child_indexes (container, actor, &sizeX, &sizeY);
  if ((sizeX + 1) * (sizeY + 1) %2 == 0) {
    selected = actor;
    on_rect_entered (actor, NULL, container);
  }
}

void initialize_size_selector (ClutterActor *actor) {
  guint i, j;
  ClutterLayoutManager * table_layout = clutter_table_layout_new ();
  sizetext = clutter_text_new();
  clutter_text_set_color (CLUTTER_TEXT (sizetext), CLUTTER_COLOR_White);
  update_size_text(sizeX, sizeY);
  ClutterColor * color = clutter_color_new (64, 64, 64, 128);
  clutter_actor_set_background_color(sizetext, color);
  clutter_color_free (color);
  clutter_table_layout_set_row_spacing (CLUTTER_TABLE_LAYOUT (table_layout), 1);
  clutter_table_layout_set_column_spacing (CLUTTER_TABLE_LAYOUT (table_layout), 1);
  ClutterActor *container = clutter_actor_new ();
  clutter_actor_set_layout_manager (container, table_layout);
  clutter_actor_add_constraint (container, clutter_bind_constraint_new (actor, CLUTTER_BIND_ALL, 0));
  clutter_actor_add_child (actor, container);
  clutter_actor_add_constraint (sizetext, clutter_align_constraint_new (actor, CLUTTER_ALIGN_BOTH, 0.5));
  clutter_actor_add_child (actor, sizetext);
  clutter_actor_set_reactive (container, TRUE);
  
  clutter_actor_set_background_color (container, CLUTTER_COLOR_Gray);
  
  for (i=0;i<MAXGRIDHEIGHT;i++) {
    for (j=0;j<MAXGRIDWIDTH;j++) {
      ClutterActor *actor = clutter_actor_new ();
      selactors[i][j] = actor;
      ClutterAction *clicked = clutter_click_action_new ();
      clutter_actor_set_background_color (actor, ((i==sizeY && j < sizeX) || (j == sizeX && i<=sizeY) || (i==0 && j < sizeX) || (j == 0 && i<=sizeY))?PRIMARY_COLOR:BACKGROUND_COLOR);
      clutter_actor_set_reactive (actor, TRUE);
      clutter_actor_add_action (actor, clicked);
      g_signal_connect (clicked, "clicked", G_CALLBACK (on_rect_clicked), container);
      g_signal_connect (actor, "enter-event", G_CALLBACK (on_rect_entered),container);
      clutter_table_layout_pack (CLUTTER_TABLE_LAYOUT (table_layout), actor, j, i);
    }
  }
  selected = selactors[sizeY][sizeX];
}

void get_selected_size (guint *width, guint *height) {
  *width = sizeX+1;
  *height = sizeY+1;
}
