/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * dripdexon-size-selector.h
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * dripdexon-size-selector.h is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * dripdexon-size-selector.h is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __DRIPDEXON_SIZE_SELECTOR_H__
#define __DRIPDEXON_SIZE_SELECTOR_H__

#include <config.h>
#include <clutter/clutter.h>
#include "dripdexon-setup.h"
#include <math.h>

void initialize_size_selector (ClutterActor *actor);
void get_selected_size (guint *width, guint *height);

#endif /*  __DRIPDEXON_SIZE_SELECTOR_H__*/
