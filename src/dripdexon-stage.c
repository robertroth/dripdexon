/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * dripdexon-stage.c
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * dripdexon-stage.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * dripdexon-stage.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dripdexon-stage.h"
#include "dripdexon-setup.h"
#include "dripdexon-card.h"
#include "dripdexon-theme.h"
#include "content-cache.h"

struct _DripdexonStagePrivate
{
  gboolean started;
  gboolean paused;
  guint flips;
  guint pairs;
  GHashTable* content_cache;
  GTimer *timer;
  DripdexonCard *last;
  guint width;
  guint height;
  DripdexonTheme *theme;
};

enum
{
  PROP_0,

  PROP_STARTED,
  PROP_PAUSED,
  PROP_FLIPS,
  PROP_PAIRS,
  PROP_ELAPSED,
  PROP_WIDTH,
  PROP_HEIGHT,
  PROP_THEME
};

enum
{
  STARTED,
  PAUSED,
  RESUMED,
  DONE,
  REVEALED,
  MATCHED,
  LAST_SIGNAL
};


static guint stage_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (DripdexonStage, dripdexon_stage, G_TYPE_OBJECT);

static void
dripdexon_stage_init (DripdexonStage *stage)
{
  DripdexonStagePrivate *priv = DRIPDEXON_STAGE_GET_PRIVATE (stage);
  priv->started = FALSE;
  priv->paused = TRUE;
  priv->flips = 0;
  priv->pairs = 0;
  priv->last = NULL;
  priv->timer = NULL;
  priv->theme = NULL;
  priv->width = GRIDWIDTH;
  priv->height = GRIDHEIGHT;
  priv->content_cache = g_hash_table_new (g_str_hash, g_str_equal);
  stage->priv = priv;
  
}

static void
dripdexon_stage_finalize (GObject *object)
{
  DripdexonStagePrivate *priv = DRIPDEXON_STAGE_GET_PRIVATE (object);
  g_hash_table_destroy (priv->content_cache);
  if (priv->timer) {
    g_timer_destroy (priv->timer);
  }
  G_OBJECT_CLASS (dripdexon_stage_parent_class)->finalize (object);
}

static void
dripdexon_stage_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
  g_return_if_fail (DRIPDEXON_IS_STAGE (object));

  switch (prop_id)
  {
  case PROP_STARTED:
    break;
  case PROP_PAUSED:
    break;
  case PROP_FLIPS:
    break;
  case PROP_PAIRS:
    break;
  case PROP_ELAPSED:
    break;
  case PROP_THEME:
    dripdexon_stage_set_theme (DRIPDEXON_STAGE (object), g_value_get_object (value));
    break;
  case PROP_WIDTH:
    dripdexon_stage_set_width (DRIPDEXON_STAGE (object), g_value_get_uint (value));
    break;
  case PROP_HEIGHT:
    dripdexon_stage_set_height (DRIPDEXON_STAGE (object), g_value_get_uint (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}

static void
dripdexon_stage_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
  g_return_if_fail (DRIPDEXON_IS_STAGE (object));
  DripdexonStage *stage = DRIPDEXON_STAGE (object);
  switch (prop_id)
  {
  case PROP_STARTED:
    g_value_set_boolean (value, stage->priv->started);
    break;
  case PROP_PAUSED:
    g_value_set_boolean (value, stage->priv->paused);
    break;
  case PROP_FLIPS:
    g_value_set_int (value, stage->priv->flips);
    break;
  case PROP_PAIRS:
    g_value_set_int (value, stage->priv->pairs);
    break;
  case PROP_ELAPSED:
    g_value_set_int (value, dripdexon_stage_get_elapsed (stage));
    break;
  case PROP_THEME:
    g_value_set_object (value, stage->priv->theme);
    break;
  case PROP_WIDTH:
    g_value_set_uint (value, stage->priv->width);
    break;
  case PROP_HEIGHT:
    g_value_set_uint (value, stage->priv->height);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}

static void
dripdexon_stage_started (DripdexonStage *self)
{
  //printf ("Game started\n");
}

static void
dripdexon_stage_paused (DripdexonStage *self)
{
  //printf ("Game paused\n");
}

static void
dripdexon_stage_resumed (DripdexonStage *self)
{
  //printf ("Game resumed\n");
}

static void
dripdexon_stage_done (DripdexonStage *self)
{
  g_timer_stop (self->priv->timer);
  self->priv->started = FALSE;
  self->priv->paused = TRUE;
  //printf ("Game finished\n");
}

static void
dripdexon_stage_revealed (DripdexonStage *self, DripdexonCard *card)
{
  //printf ("Game card %s revealed\n", clutter_actor_get_name (CLUTTER_ACTOR (card)));
}

static void
dripdexon_stage_matched (DripdexonStage *self, DripdexonCard *card)
{
  //printf ("Game cards for %s matched\n", clutter_actor_get_name (CLUTTER_ACTOR (card)));
}

static void
dripdexon_stage_class_init (DripdexonStageClass *klass)
{
  GObjectClass* object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = dripdexon_stage_finalize;
  object_class->set_property = dripdexon_stage_set_property;
  object_class->get_property = dripdexon_stage_get_property;
  g_type_class_add_private (klass, sizeof (DripdexonStagePrivate));
  
  klass->started = dripdexon_stage_started;
  klass->paused = dripdexon_stage_paused;
  klass->resumed = dripdexon_stage_resumed;
  klass->done = dripdexon_stage_done;
  klass->matched = dripdexon_stage_matched;
  klass->revealed = dripdexon_stage_revealed;
  
  g_object_class_install_property (object_class,
                                   PROP_STARTED,
                                   g_param_spec_boolean ("started",
                                                        "started",
                                                        "Whether the game has been started",
                                                        FALSE,
                                                        G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_PAUSED,
                                   g_param_spec_boolean ("paused",
                                                        "paused",
                                                        "Whether the game is currently paused",
                                                        TRUE,
                                                        G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_FLIPS,
                                   g_param_spec_int ("flips",
                                                        "flips",
                                                        "Number of cards flipped",
                                                        0, G_MAXINT32, 0,
                                                        G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_PAIRS,
                                   g_param_spec_int ("pairs",
                                                        "pairs",
                                                        "Number of pairs found",
                                                        0, G_MAXINT32, 0, 
                                                        G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_ELAPSED,
                                   g_param_spec_int ("elapsed",
                                                        "elapsed",
                                                        "Number of seconds played",
                                                        0, G_MAXINT32, 0,
                                                        G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_WIDTH,
                                   g_param_spec_uint ("width",
                                                        "width",
                                                        "Width of game board",
                                                        2, MAXGRIDWIDTH, GRIDWIDTH,
                                                        G_PARAM_READABLE | G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   PROP_HEIGHT,
                                   g_param_spec_uint ("height",
                                                        "height",
                                                        "Height of game board",
                                                        2, MAXGRIDHEIGHT, GRIDHEIGHT, 
                                                        G_PARAM_READABLE | G_PARAM_WRITABLE));
                                                        
  g_object_class_install_property (object_class,
                                   PROP_THEME,
                                   g_param_spec_object ("theme",
                                                        "theme",
                                                        "Game board theme",
                                                        DRIPDEXON_TYPE_THEME,  
                                                        G_PARAM_READABLE | G_PARAM_WRITABLE));

  stage_signals[STARTED] =
    g_signal_new ("started",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DripdexonStageClass, started),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  stage_signals[PAUSED] =
    g_signal_new ("paused",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DripdexonStageClass, paused),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  stage_signals[RESUMED] =
    g_signal_new ("resumed",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DripdexonStageClass, resumed),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
  stage_signals[DONE] =
    g_signal_new ("done",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DripdexonStageClass, done),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
  stage_signals[REVEALED] =
    g_signal_new ("revealed",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DripdexonStageClass, revealed),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRIPDEXON_TYPE_CARD);
  stage_signals[MATCHED] =
    g_signal_new ("matched",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DripdexonStageClass, matched),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, DRIPDEXON_TYPE_CARD);                  

}

static gboolean card_hide(gpointer actor)
{
  dripdexon_card_set_flipped (actor, FALSE);
  return FALSE;
}

static void flipped (DripdexonCard *card, gpointer user_data)
{
  DripdexonStage *stage = DRIPDEXON_STAGE (user_data);
  DripdexonStagePrivate *priv = stage->priv;
  if (!priv->started) {
    // on first click start the game
    priv->started = TRUE;
    // TODO emit started signal
    g_signal_emit ( stage, stage_signals[STARTED], 0);
  }
  if (priv->paused) {
    dripdexon_stage_resume (stage);
  }
  
  if (dripdexon_card_is_flipped (card)) {
    // if this is a card reveal
    // TODO emit revealed signal
    priv->flips += 1;
    g_signal_emit_by_name (stage, "revealed", card);
    // if the card flipped is the same as the last flipped, don't match
    if (priv->last && priv->last == card) {
      priv->last = NULL;
    } else if (priv->last) {
      // if we have a flipped card
      if (g_str_equal (clutter_actor_get_name (CLUTTER_ACTOR (card)), clutter_actor_get_name (CLUTTER_ACTOR (priv->last)))) {
        // if the cards match, deactivate them, and increase the score
        dripdexon_card_deactivate (priv->last);
        dripdexon_card_deactivate (card);
        priv->pairs += 1;
        // TODO emit matched signal
        g_signal_emit_by_name (stage, "matched", card);
        // TODO if score == size emit done signal
        if (priv->pairs == priv->width * priv->height / 2) {
          g_signal_emit ( stage, stage_signals[DONE], 0);
        }
      } else {
        // otherwise flip them back after a timeout
        clutter_threads_add_timeout (BACKFLIP_TIMEOUT, card_hide, priv->last);
        clutter_threads_add_timeout (BACKFLIP_TIMEOUT, card_hide, card);
      }
      // we've flipped two cards, they're either matches, or both flipped back, so we don't have a flipped card
      priv->last = NULL;
    } else {
      // we have just flipped a card
      priv->last = card;
    }
  }
}

static void generate_cards (ClutterActor *parent, DripdexonStage *ddex_stage) {
  ClutterLayoutManager *layout;
  guint i, j;
  DripdexonCard * card;
  DripdexonStagePrivate *priv = ddex_stage->priv;
  ClutterActor *stage = clutter_actor_new ();
  clutter_actor_add_child (parent, stage);
  clutter_actor_add_constraint (stage, clutter_bind_constraint_new (parent, CLUTTER_BIND_ALL, 0));
  layout = clutter_table_layout_new ();
  clutter_table_layout_set_column_spacing (CLUTTER_TABLE_LAYOUT (layout),
                                          SPACING);
  clutter_table_layout_set_row_spacing (CLUTTER_TABLE_LAYOUT (layout),
                                       SPACING);

  clutter_actor_set_layout_manager (stage, layout);
  
  GList *numbers = NULL;
  guint image_nr = dripdexon_theme_get_image_count (ddex_stage->priv->theme);
  
  const gchar *path = dripdexon_theme_get_path (ddex_stage->priv->theme);
  ClutterColor *color = dripdexon_theme_get_primary_color (ddex_stage->priv->theme);
  const gchar *theme = dripdexon_theme_get_name (ddex_stage->priv->theme);
  printf ("Theme %s has %d images, selecting %d for %dx%d grid\n", theme, image_nr, (priv->width*priv->height)/2, priv->width, priv->height);
  
  // generate the numbers of the images appearing on the table
  for (i = 0;i<(priv->width*priv->height)/2;i++) {
    guint number;
    if (image_nr > (priv->width*priv->height)/2 ) {
      // if we have more images then required, make sure each one is only used once
      do {
        //randomize a number until we find one not in the list
        number = rand()%image_nr;
      } while (g_list_find (numbers, GINT_TO_POINTER(number)) != NULL );
    } else {
      // if the grid is bigger then the numbe of images, just get an index
      number = rand()%image_nr;
    }
    // add each image index twice, for matching pairs
    numbers = g_list_append (numbers, GINT_TO_POINTER (number));
    numbers = g_list_append (numbers, GINT_TO_POINTER (number));
  }
  g_hash_table_remove_all (ddex_stage->priv->content_cache);
  for (i=0 ; i<priv->height ; i++) {
    for (j=0 ; j<priv->width ; j++) {
      // generate a random index from the above-generated list
      guint index = rand () % g_list_length (numbers);
      gpointer nth = g_list_nth_data(numbers, index);
      // use that image
      gchar *image = dripdexon_theme_get_image_at_index(ddex_stage->priv->theme, GPOINTER_TO_INT (nth));
      // and remove the index from the list, as we should not use that anymore
      numbers = g_list_remove (numbers, nth);
      // cache the image for reuse
      cache_image (ddex_stage->priv->content_cache, image, theme, path);
      card = g_object_new(DRIPDEXON_TYPE_CARD, "name", image, "content-cache", ddex_stage->priv->content_cache, "theme", ddex_stage->priv->theme, NULL);
      g_signal_connect (card, "flipped", G_CALLBACK(flipped), ddex_stage);
      clutter_table_layout_pack (CLUTTER_TABLE_LAYOUT(layout), CLUTTER_ACTOR(card), j, i);
    }
  }
  clutter_color_free(color);
}

void dripdexon_stage_initialize (DripdexonStage *stage, ClutterActor *clutter_stage) {
  DripdexonStagePrivate *priv = stage->priv;
  priv->started = FALSE;
  priv->paused = TRUE;
  priv->flips = 0;
  priv->pairs = 0;
  priv->last = NULL;
  priv->timer = g_timer_new ();
  clutter_actor_remove_all_children (clutter_stage);
  g_timer_stop (priv->timer);
  generate_cards (clutter_stage, stage);
}

void dripdexon_stage_pause (DripdexonStage *stage) {
  DripdexonStagePrivate *priv = stage->priv;
  if (stage->priv->started && !stage->priv->paused) {
    g_timer_stop (priv->timer);
    priv->paused = TRUE;
    g_signal_emit (stage, stage_signals[PAUSED], 0);
  }
}

void dripdexon_stage_resume (DripdexonStage *stage) {
  DripdexonStagePrivate *priv = stage->priv;
  if (stage->priv->started && stage->priv->paused) {
    g_timer_continue (priv->timer);
    priv->paused = FALSE;
    g_signal_emit (stage, stage_signals[RESUMED], 0);
  }
}

guint dripdexon_stage_get_elapsed (DripdexonStage *stage) {
  return stage->priv->timer ? g_timer_elapsed (stage->priv->timer, NULL) : 0;
}

gboolean dripdexon_stage_is_paused (DripdexonStage *stage) {
  return stage->priv->paused;
}

guint dripdexon_stage_get_pairs (DripdexonStage *stage) {
  return stage->priv->pairs;
}
guint dripdexon_stage_get_flips (DripdexonStage *stage) {
  return stage->priv->flips;
}

gboolean dripdexon_stage_is_started (DripdexonStage *stage) {
  return stage->priv->started;
}
 
void dripdexon_stage_set_width (DripdexonStage *stage, guint width) {
  stage->priv->width = width;
}

void dripdexon_stage_set_height (DripdexonStage *stage, guint height) {
  stage->priv->height = height;
}
void dripdexon_stage_set_theme (DripdexonStage *stage, DripdexonTheme *theme) {
  stage->priv->theme = theme;
}

void dripdexon_stage_get_size (DripdexonStage *stage, guint *width, guint *height) {
  *width = stage->priv->width;
  *height = stage->priv->height;
}

DripdexonTheme* dripdexon_stage_get_theme (DripdexonStage *stage) {
  return stage->priv->theme;
}
