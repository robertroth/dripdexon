/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * dripdexon-stage.c
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * dripdexon-stage.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * dripdexon-stage.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DRIPDEXON_STAGE_H_
#define _DRIPDEXON_STAGE_H_

#include <glib-object.h>
#include <clutter/clutter.h>
#include "dripdexon-setup.h"
#include "dripdexon-card.h"
#include "dripdexon-theme.h"

#define BACKFLIP_TIMEOUT    1000

G_BEGIN_DECLS

#define DRIPDEXON_TYPE_STAGE             (dripdexon_stage_get_type ())
#define DRIPDEXON_STAGE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRIPDEXON_TYPE_STAGE, DripdexonStage))
#define DRIPDEXON_STAGE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), DRIPDEXON_TYPE_STAGE, DripdexonStageClass))
#define DRIPDEXON_IS_STAGE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRIPDEXON_TYPE_STAGE))
#define DRIPDEXON_IS_STAGE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), DRIPDEXON_TYPE_STAGE))
#define DRIPDEXON_STAGE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), DRIPDEXON_TYPE_STAGE, DripdexonStageClass))
#define DRIPDEXON_STAGE_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), DRIPDEXON_TYPE_STAGE, DripdexonStagePrivate))

typedef struct _DripdexonStageClass DripdexonStageClass;
typedef struct _DripdexonStage DripdexonStage;
typedef struct _DripdexonStagePrivate DripdexonStagePrivate;

struct _DripdexonStageClass
{
  GObjectClass parent_class;

  void(* started) (DripdexonStage *self);
  void(* paused) (DripdexonStage *self);
  void(* resumed) (DripdexonStage *self);
  void(* done) (DripdexonStage *self);
  void(* revealed) (DripdexonStage *self, DripdexonCard *card);
  void(* matched) (DripdexonStage *self, DripdexonCard *card);
};

struct _DripdexonStage
{
  GObject parent_instance;
  DripdexonStagePrivate *priv;
 
};

// generate the cards, initialize score, time, everything
void dripdexon_stage_initialize (DripdexonStage *stage, ClutterActor *clutter_stage);
// pause the game
void dripdexon_stage_pause (DripdexonStage *stage);
// resume the game
void dripdexon_stage_resume (DripdexonStage *stage);

guint dripdexon_stage_get_elapsed (DripdexonStage *stage);
guint dripdexon_stage_get_pairs (DripdexonStage *stage);
guint dripdexon_stage_get_flips (DripdexonStage *stage);
DripdexonTheme* dripdexon_stage_get_theme (DripdexonStage *stage);
void dripdexon_stage_get_size (DripdexonStage *stage, guint *width, guint *height);
gboolean dripdexon_stage_is_paused (DripdexonStage *stage);
gboolean dripdexon_stage_is_started (DripdexonStage *stage);
void dripdexon_stage_set_width (DripdexonStage *stage, guint width);
void dripdexon_stage_set_height (DripdexonStage *stage, guint height);
void dripdexon_stage_set_theme (DripdexonStage *stage, DripdexonTheme* theme);

GType dripdexon_stage_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* _DRIPDEXON_STAGE_H_ */

