/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * dripdexon-theme.c
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * dripdexon-theme.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * dripdexon-theme.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dripdexon-theme.h"
#include "dripdexon-setup.h"
      
                      
struct _DripdexonThemePrivate
{
  gchar *name;
  gchar *path;
  ClutterColor *primary;
  ClutterColor *secondary;
  ClutterColor *background;
  GSList *images;
};

enum
{
  PROP_0,

  PROP_NAME,
  PROP_PATH,
  PROP_IMAGES,
  PROP_PRIMARY,
  PROP_SECONDARY,
  PROP_BACKGROUND,
  PROP_LAST
};

G_DEFINE_TYPE (DripdexonTheme, dripdexon_theme, G_TYPE_OBJECT);

static void
dripdexon_theme_init (DripdexonTheme *theme)
{
  DripdexonThemePrivate *priv = DRIPDEXON_THEME_GET_PRIVATE (theme);
  priv->name = NULL;
  priv->path = NULL;
  priv->images = NULL;
  priv->primary = clutter_color_copy(CLUTTER_COLOR_Black);
  priv->secondary = clutter_color_copy(CLUTTER_COLOR_White);
  priv->background = clutter_color_copy(CLUTTER_COLOR_Gray);
  theme->priv = priv;
}

static void
dripdexon_theme_finalize (GObject *object)
{
  DripdexonThemePrivate *priv = DRIPDEXON_THEME_GET_PRIVATE (object);
  G_OBJECT_CLASS (dripdexon_theme_parent_class)->finalize (object);
}

void dripdexon_theme_set_name (DripdexonTheme* theme, gchar* name) {
  if (g_strcmp0 (theme->priv->name, name) == 0 ) {
    g_free (name);
  }
  if (theme->priv->name) {
    g_free (theme->priv->name);
  }
  theme->priv->name = g_strdup(name);
}

void dripdexon_theme_set_path (DripdexonTheme* theme, gchar* path) {
    if (g_strcmp0 (theme->priv->path, path) == 0 ) {
    g_free (path);
  }
  if (theme->priv->path) {
    g_free (theme->priv->path);
  }
  theme->priv->path = g_strdup(path);
}
void dripdexon_theme_set_primary_color (DripdexonTheme* theme, ClutterColor *color) {
  if (clutter_color_equal(color, theme->priv->primary)) {
    clutter_color_free (color);
    return;
  }
  clutter_color_free(theme->priv->primary);
  theme->priv->primary = clutter_color_copy (color);
}

void dripdexon_theme_set_secondary_color (DripdexonTheme* theme, ClutterColor *color) {
  if (clutter_color_equal(color, theme->priv->secondary)) {
    clutter_color_free (color);
    return;
  }
  clutter_color_free(theme->priv->secondary);
  theme->priv->secondary = clutter_color_copy (color);
}
void dripdexon_theme_set_background_color (DripdexonTheme* theme, ClutterColor *color) {
  if (clutter_color_equal(color, theme->priv->background)) {
    clutter_color_free (color);
    return;
  }
  clutter_color_free(theme->priv->background);
  theme->priv->background = clutter_color_copy (color);
}
void dripdexon_theme_set_images (DripdexonTheme* theme, GSList *images) {
  theme->priv->images = images;
}

static void
dripdexon_theme_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
  g_return_if_fail (DRIPDEXON_IS_THEME (object));
  DripdexonTheme* theme = DRIPDEXON_THEME (object);
  switch (prop_id)
  {
  case PROP_NAME:
    
    dripdexon_theme_set_name (theme, g_value_get_string (value));
    break;
  case PROP_PATH:
    dripdexon_theme_set_path (theme, g_value_get_string (value));
    break;
  case PROP_IMAGES:
    dripdexon_theme_set_images (theme, g_value_get_pointer (value));
    break;
  case PROP_PRIMARY:
    dripdexon_theme_set_primary_color (theme, g_value_get_boxed (value));
    break;
  case PROP_SECONDARY:
    dripdexon_theme_set_secondary_color (theme, g_value_get_boxed (value));
    break;
  case PROP_BACKGROUND:
    dripdexon_theme_set_background_color (theme, g_value_get_boxed (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}

static void
dripdexon_theme_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
  g_return_if_fail (DRIPDEXON_IS_THEME (object));
  DripdexonTheme *theme = DRIPDEXON_THEME (object);
  switch (prop_id)
  {
  case PROP_NAME:
    g_value_set_string (value, theme->priv->name);
    break;
  case PROP_PATH:
    g_value_set_string (value, theme->priv->path);
    break;
  case PROP_IMAGES:
    g_value_set_pointer (value, theme->priv->images);
    break;
  case PROP_PRIMARY:
    g_value_set_boxed (value, dripdexon_theme_get_primary_color(theme));
    break;
  case PROP_SECONDARY:
    g_value_set_boxed (value, dripdexon_theme_get_secondary_color(theme));
    break;
  case PROP_BACKGROUND:
    g_value_set_boxed (value, dripdexon_theme_get_background_color(theme));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}

static void
dripdexon_theme_class_init (DripdexonThemeClass *klass)
{
  GObjectClass* object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = dripdexon_theme_finalize;
  object_class->set_property = dripdexon_theme_set_property;
  object_class->get_property = dripdexon_theme_get_property;
  g_type_class_add_private (klass, sizeof (DripdexonThemePrivate));
  
  g_object_class_install_property (object_class,
                                   PROP_NAME,
                                   g_param_spec_string ("name",
                                                        "name",
                                                        "The name of theme",
                                                        "",
                                                        G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property (object_class,
                                   PROP_PATH,
                                   g_param_spec_string ("path",
                                                        "path",
                                                        "The path of theme folder",
                                                        NULL,
                                                        G_PARAM_READABLE | G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   PROP_IMAGES,
                                   g_param_spec_pointer ("images",
                                                        "images",
                                                        "The list of images in this theme",
                                                        G_PARAM_READABLE | G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   PROP_PRIMARY,
                                   g_param_spec_boxed ("primary",
                                                        "primary",
                                                        "Primary color of the theme",
                                                        CLUTTER_TYPE_COLOR,
                                                        G_PARAM_READABLE | G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   PROP_SECONDARY,
                                   g_param_spec_boxed ("secondary",
                                                        "secondary",
                                                        "Secondary color of the theme",
                                                        CLUTTER_TYPE_COLOR,
                                                        G_PARAM_READABLE | G_PARAM_WRITABLE));    
  g_object_class_install_property (object_class,
                                   PROP_BACKGROUND,
                                   g_param_spec_boxed ("background",
                                                        "background",
                                                        "Background color of the theme",
                                                        CLUTTER_TYPE_COLOR,
                                                        G_PARAM_READABLE | G_PARAM_WRITABLE));

}

gchar* dripdexon_theme_get_name (DripdexonTheme* theme) {
   return theme->priv->name;
}
gchar* dripdexon_theme_get_path (DripdexonTheme* theme) {
  return theme->priv->path;
}

guint dripdexon_theme_get_image_count (DripdexonTheme* theme) {
  return g_slist_length (theme->priv->images);
}

gchar *dripdexon_theme_get_image_at_index (DripdexonTheme* theme, guint index) {
  return g_slist_nth_data (theme->priv->images, index);
}

ClutterColor *dripdexon_theme_get_primary_color (DripdexonTheme* theme) {
  return clutter_color_copy(theme->priv->primary);
}

ClutterColor *dripdexon_theme_get_secondary_color (DripdexonTheme* theme) {
  return clutter_color_copy(theme->priv->secondary);
}
ClutterColor *dripdexon_theme_get_background_color (DripdexonTheme* theme) {
  return clutter_color_copy(theme->priv->background);
}

DripdexonTheme * dripdexon_theme_new_for_folder (gchar* path) {
  return g_object_new (DRIPDEXON_TYPE_THEME, "path", path, NULL);
}

DripdexonTheme * dripdexon_theme_new_with_settings (gchar* name, gchar* path, GSList *images, const ClutterColor *primary, const ClutterColor *secondary, const ClutterColor *background) {
  return g_object_new (DRIPDEXON_TYPE_THEME, "name", name, "path", path, "images", images, 
                                             "primary", primary, 
                                             "secondary", secondary, 
                                             "background", background, NULL);
                                             
}

GdkPixbuf * dripdexon_theme_get_preview_image (DripdexonTheme *theme) {
  gchar* image = g_slist_nth_data (theme->priv->images, rand() % g_slist_length(theme->priv->images));
  gchar* filename = g_build_filename(theme->priv->path, image, NULL);
  //RsvgHandle* svg= rsvg_handle_new_from_file (filename, NULL);
  GdkPixbuf *result = rsvg_pixbuf_from_file_at_max_size (filename, 64, 64, NULL);
  //rsvg_handle_close (svg, NULL);
  return result;
}
