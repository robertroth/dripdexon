/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * dripdexon-theme.h
 * Copyright (C) 2013 Robert Roth <robert.roth.off@gmail.com>
 * 
 * dripdexon-theme.h is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * dripdexon-theme.h is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __DRIPDEXON_THEME_H__
#define __DRIPDEXON_THEME_H__

#include <config.h>
#include <glib-object.h>
#include <clutter/clutter.h>
#include <librsvg/rsvg.h>

G_BEGIN_DECLS

#define DRIPDEXON_TYPE_THEME                  (dripdexon_theme_get_type ())
#define DRIPDEXON_THEME(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), DRIPDEXON_TYPE_THEME, DripdexonTheme))
#define DRIPDEXON_IS_THEME(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DRIPDEXON_TYPE_THEME))
#define DRIPDEXON_THEME_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), DRIPDEXON_TYPE_THEME, DripdexonThemeClass))
#define DRIPDEXON_IS_THEME_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), DRIPDEXON_TYPE_THEME))
#define DRIPDEXON_THEME_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), DRIPDEXON_TYPE_THEME, DripdexonThemeClass))
#define DRIPDEXON_THEME_GET_PRIVATE(obj)      (G_TYPE_INSTANCE_GET_PRIVATE ((obj), DRIPDEXON_TYPE_THEME, DripdexonThemePrivate))

typedef struct _DripdexonTheme   DripdexonTheme;
typedef struct _DripdexonThemeClass   DripdexonThemeClass;
typedef struct _DripdexonThemePrivate   DripdexonThemePrivate;

struct _DripdexonTheme
{
  GObject parent_instance;
  DripdexonThemePrivate *priv;
};

struct _DripdexonThemeClass
{
  GObjectClass parent_class;
};

GType dripdexon_theme_get_type(void);

DripdexonTheme * dripdexon_theme_new_for_folder (gchar* path);
DripdexonTheme * dripdexon_theme_new_with_settings (gchar* name, gchar* path, GSList *images, const ClutterColor *primary, const ClutterColor *secondary, const ClutterColor *background);

gchar* dripdexon_theme_get_name (DripdexonTheme* theme);
gchar* dripdexon_theme_get_path (DripdexonTheme* theme);
guint dripdexon_theme_get_image_count (DripdexonTheme* theme);
gchar *dripdexon_theme_get_image_at_index (DripdexonTheme* theme, guint index);
ClutterColor *dripdexon_theme_get_primary_color (DripdexonTheme* theme);
ClutterColor *dripdexon_theme_get_secondary_color (DripdexonTheme* theme);
ClutterColor *dripdexon_theme_get_background_color (DripdexonTheme* theme);

void dripdexon_theme_set_name (DripdexonTheme* theme, gchar* name);
void dripdexon_theme_set_path (DripdexonTheme* theme, gchar* path);
void dripdexon_theme_set_primary_color (DripdexonTheme* theme, ClutterColor *color);
void dripdexon_theme_set_secondary_color (DripdexonTheme* theme, ClutterColor *color);
void dripdexon_theme_set_background_color (DripdexonTheme* theme, ClutterColor *color);
void dripdexon_theme_set_images (DripdexonTheme* theme, GSList *images);

GdkPixbuf * dripdexon_theme_get_preview_image (DripdexonTheme *theme);

G_END_DECLS

#endif /*  __DRIPDEXON_THEME_H__ */
